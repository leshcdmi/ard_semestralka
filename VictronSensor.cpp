#pragma once
#include <Arduino.h>
#include <ArduinoJson.h>
#include "constants.h"

class VictronSensor{
  private:
    double table[VICTRON_TABLE_SIZE];
    HardwareSerial hardwareSerial;

    String readUntil(String separator){
      String output = "";
      
      while(true){
        if(!hardwareSerial.available()) continue;
        if(output.endsWith(separator)){
          output.remove(output.length() - separator.length(), separator.length());
          return output;
        }    
        output += (char) hardwareSerial.read();
      }  
    }
    bool checkSum(String content){
      int sum = 0;
      for(unsigned int i = 0; i < content.length(); i++){
        sum += (char) content.charAt(i);
      }
      Serial.println("Sum: " + sum);
      Serial.println("Modul: " + (sum % 256));
      return sum % 256 == 0;      
    }
    int getIndexByName(String name){
      if(name == "PID") return PID;
      if(name == "FW") return FW;
      if(name == "SER#") return SER;
      if(name == "V") return _V;
      if(name == "I") return I;
      if(name == "VPV") return VPV;
      if(name == "PPV") return PPV;
      if(name == "CS") return CS;
      if(name == "MPPT") return MPPT;
      if(name == "OR") return OR;
      if(name == "ERR") return ERR;
      if(name == "LOAD") return LOAD;
      if(name == "H19") return H19;
      if(name == "H20") return H20;
      if(name == "H21") return H21;
      if(name == "H22") return H22;
      if(name == "H23") return H23;
      if(name == "HSDS") return HSDS;
      if(name == "Checksum") return CHECKSUM;
      return -1;
    }
  public:
    VictronSensor():hardwareSerial(1){
      for(int i = 0; i < VICTRON_TABLE_SIZE; i++){
        this->table[i] = 0.0;                
      }
      hardwareSerial.begin(VICTRON_BAUDRATE, SERIAL_8N1, VICTRON_RX, VICTRON_TX);      
    }
    ~VictronSensor(){};
    bool read(){
      String newTable[VICTRON_TABLE_SIZE];
      String content = "";
      
      while(true){
        String fieldLabelString = readUntil("\t");
        String fieldValueString = readUntil("\r\n");
        content += fieldLabelString + "\t" + fieldValueString + "\r\n";

        int index = getIndexByName(fieldLabelString);
        if(index < 0 || index > CHECKSUM) continue;
        
        newTable[index] = fieldValueString;
        if(index == CHECKSUM) break;
      } 

      if(checkSum(content)){
        for(int i = 0; i < VICTRON_TABLE_SIZE; i++){
          this->table[i] = newTable[i].toDouble();
        }
        return true;
      }
      return false;
    }
    const double *getTable() const{
      return this->table;
    }
    const size_t getTableSize() const{
      return VICTRON_TABLE_SIZE;
    }
};
