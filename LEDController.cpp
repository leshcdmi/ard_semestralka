#pragma once
#include <Arduino.h>
#include <ArduinoJson.h>
#include "constants.h"
#include "SensorState.cpp"
#include "SPIFFS.h"
#include "tinyexpr.h"

class LEDController{
  private:
    String equation;
    String compileEquation(SensorState& sensors){
      StaticJsonDocument<400> doc; // https://arduinojson.org/v6/assistant
      deserializeJson(doc, sensors.toJson());
      JsonObject documentRoot = doc.as<JsonObject>();
      
      String compiledEquation = equation;
      for(JsonPair pair: documentRoot){
        String tmp(pair.key().c_str());
        tmp.toUpperCase();
        compiledEquation.replace(tmp, pair.value().as<String>());
      }
      return compiledEquation;
    }
  public:
    LEDController(){
      // pinMode(PIN_LED_R, OUTPUT);
      pinMode(PIN_LED_G, OUTPUT);
      pinMode(PIN_LED_B, OUTPUT);
      digitalWrite(PIN_LED_G, LOW);
      digitalWrite(PIN_LED_B, LOW);
      
      ledcSetup(0, 1000, 8);
      ledcAttachPin(PIN_LED_R, 0);
      
      File equationFile = SPIFFS.open("/equation", "r");
      if (equationFile){
        equation = equationFile.readStringUntil('\n');
      }else{
        equation = "";
      }
    }
    ~LEDController(){};
    void setEquation(String& newEquation){
      if(newEquation.length() == 0) return;

      this->equation = newEquation;
      File file = SPIFFS.open("/equation", "w");
      file.print(this->equation + "\n");
      file.close();
    }
    void updateLed(SensorState& sensors){
      if(equation.length() == 0) return;
      int error;
      double result = te_interp(this->compileEquation(sensors).c_str(), &error); 
      if(error > 0) return;
      double brightness = map(constrain(result, 0, 100), 0, 100, 0, 255);  
      ledcWrite(0, brightness);
    }
};
