#pragma once

// GPIO and I2C config
#define PIN_LED_R        21
#define PIN_LED_G        15
#define PIN_LED_B        5
#define I2C_SDA          25
#define I2C_SCL          14

// VICTRON comunication config
#define VICTRON_BAUDRATE 19200
#define VICTRON_RX       22
#define VICTRON_TX       23

// WIFI access point config
#define WIFI_SSID        "ESP32-Access-Point"

// Server config
#define SERVER_PORT      80


// some constants for code
#define VICTRON_TABLE_SIZE       19
#define TABLE_SIZE               23

#define PID          0
#define FW           1
#define SER          2
#define _V            3
#define I            4
#define VPV          5
#define PPV          6
#define CS           7
#define MPPT         8
#define OR           9
#define ERR          10
#define LOAD         11
#define H19          12
#define H20          13
#define H21          14
#define H22          15
#define H23          16
#define HSDS         17
#define CHECKSUM     18

#define SENSOR_LIGHT_LUX             19
#define SENSOR_AIR_CO2               20
#define SENSOR_AIR_TEMP              21
#define SENSOR_AIR_HUMIDITY          22
