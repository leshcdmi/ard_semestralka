#include "WiFi.h"
#include "ESPAsyncWebServer.h"
#include "SPIFFS.h"

#include <Wire.h>

#include "SparkFun_SCD30_Arduino_Library.h"
#include <SparkFun_VEML7700_Arduino_Library.h>

#include "constants.h"
#include "VictronSensor.cpp"
#include "SensorState.cpp"
#include "LEDController.cpp"

SCD30 airSensor;
VEML7700 ambientSensor;

TwoWire myI2C = TwoWire(0);
AsyncWebServer server(SERVER_PORT);
AsyncWebSocket ws("/ws");
AsyncWebSocketClient* wsClient;
bool isSetupMode = true;

SensorState sensorState;
VictronSensor victronSensor;
LEDController ledController;
String acceptedPaths[] = {
  // CSS
  "/css/bootstrap.min.css",
  "/css/style.css",

  // JS
  "/js/bootstrap.bundle.min.js",    
  "/js/config.js",    
  "/js/color-modes.js",    
  "/js/index.js",    
  "/js/setup.js",  

  // ICON
  "/favicon.ico"
};

std::tuple<bool, String, String> getCredentials();

void setup(){
  // Serial port for debugging purposes
  Serial.begin(115200);
  myI2C.begin(I2C_SDA, I2C_SCL, 400000);

  setupSensors();
  setupServer();
  
}

void setupSensors(){
  if (airSensor.begin(myI2C) == false){
    Serial.println("Air sensor not detected. Please check wiring. Freezing...");
    while (1);
  }

  if (ambientSensor.begin(myI2C) == false)
  {
    Serial.println("Unable to communicate with the VEML7700. Please check the wiring. Freezing...");
    while (1);
  }
  
  // Initialize SPIFFS
  if(!SPIFFS.begin(true)){
    Serial.println("An Error has occurred while mounting SPIFFS");
    while (1);
  }
}

void setupServer(){
  for(String path: acceptedPaths){
    server.on(path.c_str(), HTTP_GET, [](AsyncWebServerRequest *request){
      request->send(SPIFFS, request->url());
    });
  }

  String ssid, password;
  std::tie(isSetupMode, ssid, password) = getCredentials();
  if(isSetupMode) setupSettingsServer();
  else setupNormalServer(ssid, password);
//  isSetupMode = false;
//  setupNormalServer("CVUT_FIT", "");

  IPAddress IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);
}

void setupSettingsServer(){
  WiFi.mode(WIFI_AP);
  WiFi.softAP(WIFI_SSID, "");

  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/setup.html");
  });
  server.on("/setupAccessPoint", HTTP_POST, [](AsyncWebServerRequest *request){
    String ssid = "";
    String password = "";

    if(request->hasParam("ssid", true)) 
      ssid = request->getParam("ssid", true)->value();
    else ssid = WIFI_SSID; 

    if(request->hasParam("password", true)) 
      password = request->getParam("password", true)->value();
      
    File file = SPIFFS.open("/credentials", "w");
    file.print(ssid + "\n" + password + "\n");
    file.close();
    request->send(200, "text/plain", "OK");
    ESP.restart();
  });
  
  // Start server
  server.begin();
}
void setupNormalServer(String ssid, String password){
  WiFi.mode(WIFI_AP);
  WiFi.softAP(ssid, password);

  File file = SPIFFS.open("/equation", "r");
  String equation = file.readStringUntil('\n');
  file.close();
  ledController.setEquation(equation);

  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/index.html");
  });
  server.on("/resetAccessPoint", HTTP_POST, [](AsyncWebServerRequest *request){
    File file = SPIFFS.open("/credentials", "w");
    file.print("");
    file.close();
    request->send(200, "text/plain", "OK");
    ESP.restart();
  });
  server.on("/getEquation", HTTP_GET, [](AsyncWebServerRequest *request){
    File file = SPIFFS.open("/equation", "r");
    String equation = file.readStringUntil('\n');
    file.close();
    request->send(200, "text/plain", equation);
  });
  server.on("/setupEquation", HTTP_POST, [](AsyncWebServerRequest *request){
    String equation = "";

    if(request->hasParam("equation", true)) 
      equation = request->getParam("equation", true)->value();
    else{
      request->send(400, "text/plain", "Param equation must be in request");
      return;
    }

    if(equation.length() == 0){
      request->send(400, "text/plain", "Equation is empty");
      return;      
    }
    
    ledController.setEquation(equation);
    request->send(200, "text/plain", "OK");
  });

  // Start server
  ws.onEvent(onWsEvent);
  server.addHandler(&ws);
  server.begin();
}

std::tuple<bool, String, String> getCredentials(){
  File credentials = SPIFFS.open("/credentials", "r");
  if (!credentials) return std::make_tuple(true, "", "");
  
  String ssid = credentials.readStringUntil('\n');
  String password = credentials.readStringUntil('\n');

  if(ssid.length() == 0) return std::make_tuple(true, "", "");
  return std::make_tuple(false, ssid, password);
}

void onWsEvent(AsyncWebSocket * server, AsyncWebSocketClient * client, AwsEventType type, void * arg, uint8_t *data, size_t len){
  if(type == WS_EVT_CONNECT){
    wsClient = client;
  } else if(type == WS_EVT_DISCONNECT){
    wsClient = nullptr;
  }
}
 
void loop(){
  if(isSetupMode) return;

  ledController.updateLed(sensorState);    
  sensorState.setUpdated(false);

  sensorState.setLightLux(ambientSensor.getLux());
  
  if (airSensor.dataAvailable()){
    sensorState.setAirCo2(airSensor.getCO2());
    sensorState.setAirTemp(airSensor.getTemperature());
    sensorState.setAirHumidity(airSensor.getHumidity());
  }
  
  if(victronSensor.read()){
    sensorState.setTable(victronSensor.getTable(), victronSensor.getTableSize());    
  }
  
  if(sensorState.isUpdated()){
    String json = sensorState.toJson();
    if(wsClient != nullptr && wsClient->canSend()) {
      wsClient->text(json);
    }
  }
}
