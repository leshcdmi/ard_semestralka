#pragma once
#include <Arduino.h>
#include <ArduinoJson.h>
#include "constants.h"

class SensorState{
  private:
    double table[TABLE_SIZE];
    bool _isUpdated = false;

    bool updateValue(const size_t index, const double newValue){
      if(TABLE_SIZE <= index) return false;
      
      long long ai = newValue * 1000;
      long long bi = this->table[index] * 1000;
      if(ai == bi) return false;
            
      this->table[index] = newValue;
      this->_isUpdated = true;
      return true;            
    }
  public:
    SensorState(){
      for(int i = 0; i < TABLE_SIZE; i++){
        this->table[i] = 0.0;                
      }
    }
    ~SensorState(){};
    void setUpdated(bool value){
      this->_isUpdated = value;
    }
    bool isUpdated(){
      return this->_isUpdated;
    }
    void setTable(const double *newTable, const size_t size){
      this->_isUpdated = true;

      for(size_t i = 0; i < size; i++)
        this->table[i] = newTable[i];
    }
    double getLightLux(){
      return this->table[SENSOR_LIGHT_LUX];
    }
    bool setLightLux(const double value){
      return this->updateValue(SENSOR_LIGHT_LUX, value);
    }
    double getAirCo2(){
      return this->table[SENSOR_AIR_CO2];
    }
    bool setAirCo2(const double value){
      return this->updateValue(SENSOR_AIR_CO2, value);
    }
    double getAirTemp(){
      return this->table[SENSOR_AIR_TEMP];
    }
    bool setAirTemp(const double value){
      return this->updateValue(SENSOR_AIR_TEMP, value);
    }
    double getAirHumidity(){
      return this->table[SENSOR_AIR_HUMIDITY];
    }
    bool setAirHumidity(const double value){
      return this->updateValue(SENSOR_AIR_HUMIDITY, value);
    }
    String toJson(){
      String json = "";
      StaticJsonDocument<400> doc; // https://arduinojson.org/v6/assistant

      doc["pid"]  = this->table[PID];
      doc["fw"]   = this->table[FW];
      doc["ser"]  = this->table[SER];
      doc["v"]    = this->table[_V];
      doc["v"]    = this->table[PID];
      doc["i"]    = this->table[I];
      doc["vpv"]  = this->table[VPV];
      doc["ppv"]  = this->table[CS];
      doc["cs"]   = this->table[PPV];
      doc["mppt"] = this->table[MPPT];
      doc["or"]   = this->table[OR];
      doc["err"]  = this->table[ERR];
      doc["load"] = this->table[LOAD];
      doc["h19"]  = this->table[H19];
      doc["h20"]  = this->table[H20];
      doc["h21"]  = this->table[H21];
      doc["h22"]  = this->table[H22];
      doc["h23"]  = this->table[H23];
      doc["hsds"] = this->table[HSDS];

      doc["lux"] = this->getLightLux();
      doc["co2"] = this->getAirCo2();
      doc["tem"] = this->getAirTemp();
      doc["hum"] = this->getAirHumidity();

      serializeJson(doc, json); 
      return json;     
    }
};
