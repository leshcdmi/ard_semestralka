const config = {
    url: {
        post: {
            "resetAccessPoint": "/resetAccessPoint",
            "setupAccessPoint": "/setupAccessPoint",
            "setupEquation": "/setupEquation"
        },
        get: {
            "getEquation": "/getEquation"
        }
    },
    webSocket:{
        url: "/ws",
    }
}