class SetupAccessPointComponent{
    constructor(formId){
        this.formDOM = document.querySelector(formId);
        this.btnDOM = this.formDOM.querySelector("button");
        this.ssidDOM = this.formDOM.querySelector("input#ssid");
        this.passwordDOM = this.formDOM.querySelector("input#password");
        this.isSecureDOM = this.formDOM.querySelector("input#isSecure");
        this.alertDOM = this.formDOM.querySelector(".alert");
        
        this.isSecureDOM.addEventListener("change", () => {
            this.passwordDOM.disabled = this.isSecureDOM.checked;
        });

        this.btnDOM.addEventListener("click", (event) => {
            event.preventDefault();
            this.hideError();
            let err = this.validate();
            if(err){
                this.showError(err);
                return;
            }
            fetch(config.url.post.setupAccessPoint, {
                method: 'POST',
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                },
                body: `ssid=${this.ssidDOM.value}&password=${this.passwordDOM.value}`
            }).then(function(response){ 
                new bootstrap.Modal('#modalOK').show();
            }).catch(function(response){ 
                new bootstrap.Modal('#modalERROR').show();
            })
        });
    }

    validate(){
        if(!this.ssidDOM.value)
            return "SSID is empty";
        if(!this.isSecureDOM.checked && !this.passwordDOM.value)
            return "Password is empty";
    }

    showError(msg){
        this.alertDOM.innerText = msg;
        this.alertDOM.classList.remove("d-none");
    }

    hideError(){
        this.alertDOM.innerText = "";
        this.alertDOM.classList.add("d-none");
    }
}

const setupAccessPointComponent = new SetupAccessPointComponent("#form");