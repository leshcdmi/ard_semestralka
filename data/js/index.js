class ResetModalComponent{
    constructor(modalId){
        this.modalDOM = document.querySelector(modalId);
        this.btnDOM = this.modalDOM.querySelector("button#btnResetAccess");

        this.btnDOM.addEventListener("click", () => {
            fetch(config.url.post.resetAccessPoint, {
                method: 'POST'
            }).then(function(response){ 
                new bootstrap.Modal('#modalOK').show();
            }).catch(function(response){ 
                new bootstrap.Modal('#modalERROR').show();
            })
        });

        this.modalDOM.addEventListener('hidden.bs.modal', () => {
            this.btnDOM.disabled = true;
        });
    }
}

class WebSocketConnection{
    constructor(){
        this.listeners = {
            open: [],
            message: [],
            error: [],
            close: [],
        };
        this.hostname = window.location.hostname;
        this.socket = new WebSocket(`ws://${this.hostname}${config.webSocket.url}`);

        this.socket.addEventListener("open", () => {
            this.listeners["open"].forEach((fn) => fn());
        });
        
        this.socket.addEventListener("message", (event) => {
            const json = JSON.parse(event.data);
            this.listeners["message"].forEach((fn) => fn(json));
        });

        this.socket.addEventListener("error", () => {
            this.listeners["error"].forEach((fn) => fn());
        });

        this.socket.addEventListener("close", () => {
            this.listeners["close"].forEach((fn) => fn());
        });
    }
    addEventListener(type, callback){
        if(!(type in this.listeners))
            throw new Error(`Listener type '${type}' is not known.`);
        this.listeners[type].push(callback);
    }

    getReadyState(){
        return this.socket.readyState;
    }
}

class Equation{
    constructor(equationId){
        this.equationDOM = document.querySelector(equationId);
        this.btnSaveDOM = this.equationDOM.querySelector("button");
        this.inputDOM = this.equationDOM.querySelector("input");

        this.btnSaveDOM.addEventListener("click", () => {
            fetch(config.url.post.setupEquation, {
                method: 'POST',
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                },
                body: `equation=${encodeURIComponent(this.inputDOM.value)}`
            }).then(this.updateCurrent)
        })

        this.updateCurrent();
    }

    updateCurrent(){
        fetch(config.url.get.getEquation)
        .then((response) => response.text())
        .then((text) => { 
            document.querySelector("#equationCurrent").innerText = "Current: " + text;
        });
    }
}

class SensorsTable{
    constructor(webSocketConection, tableId){
        this.tableDOM = document.querySelector(tableId);
        this.webSocketConection = webSocketConection;
        this.webSocketConection.addEventListener("message", this.onJson.bind(this));
        this.webSocketConection.addEventListener("open", this.onOpen);
        this.webSocketConection.addEventListener("error", this.onClose);
        this.webSocketConection.addEventListener("close", this.onClose);
    }

    onJson(json){
        let trs = this.tableDOM.querySelectorAll("tbody tr");
        Array.from(trs).forEach(tr => {
            let tds = Array.from(tr.querySelectorAll("td"));
            tds[1].innerText = parseFloat(json[tds[0].innerText.toLowerCase()]).toFixed(1);
        })
    }
    onOpen(){
        document.querySelector("#status").classList.remove("d-none");
        document.querySelector("#status").style.color = 'green';
    }
    onClose(){
        document.querySelector("#status").classList.remove("d-none");
        document.querySelector("#status").style.color = 'red';
    }
}

const resetModalComponent = new ResetModalComponent("#resetAccessModal");
const webSocketConection = new WebSocketConnection();
const sensorsTable = new SensorsTable(webSocketConection, "#sensorsTable");
const equation = new Equation("#equation");